CREATE TABLE Users (
idUser SERIAL PRIMARY KEY,
name VARCHAR(50),
surname VARCHAR(50),
mail VARCHAR(100)
);

INSERT INTO Users (name, surname, mail) VALUES
('John', 'Doe', 'john@example.com'),
('Jane', 'Smith', 'jane@example.com'),
('Alice', 'Johnson', 'alice@example.com'),
('Bob', 'Williams', 'bob@example.com'),
('Emily', 'Brown', 'emily@example.com'),
('Michael', 'Davis', 'michael@example.com'),
('Sophia', 'Wilson', 'sophia@example.com'),
('William', 'Taylor', 'william@example.com'),
('Olivia', 'Anderson', 'olivia@example.com'),
('James', 'Martinez', 'james@example.com'),
('Emma', 'Garcia', 'emma@example.com'),
('Daniel', 'Rodriguez', 'daniel@example.com');