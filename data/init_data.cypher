CREATE (:Motor {idMotor: 1, nameMotor: 'Motor A', power: 'High', priceMotor: 5000})
CREATE (:Motor {idMotor: 2, nameMotor: 'Motor B', power: 'Low', priceMotor: 3000})
CREATE (:Motor {idMotor: 3, nameMotor: 'Motor C', power: 'Medium', priceMotor: 4000})


CREATE (:Fuel {idFuel: 1, nameFuel: 'Gasoline', isGreen: false})
CREATE (:Fuel {idFuel: 2, nameFuel: 'Electric', isGreen: true})
CREATE (:Fuel {idFuel: 3, nameFuel: 'Hybrid', isGreen: true})


CREATE (:Color {idColor: 1, nameColor: 'Red'})
CREATE (:Color {idColor: 2, nameColor: 'Blue'})
CREATE (:Color {idColor: 3, nameColor: 'Green'})
CREATE (:Color {idColor: 4, nameColor: 'Yellow'})
CREATE (:Color {idColor: 5, nameColor: 'Black'})
CREATE (:Color {idColor: 6, nameColor: 'White'})


CREATE (:Model {idModel: 1, nameModel: 'Model X', priceModel: 35000})
CREATE (:Model {idModel: 2, nameModel: 'Model Y', priceModel: 45000})
CREATE (:Model {idModel: 3, nameModel: 'Model Z', priceModel: 40000})
CREATE (:Model {idModel: 4, nameModel: 'Model W', priceModel: 30000})
CREATE (:Model {idModel: 5, nameModel: 'Model V', priceModel: 32000})


