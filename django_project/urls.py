from django.contrib import admin
from django.urls import path
from .views import home, about, list_users,test_html
from .views import index_command,get_data_command,create_command,edit_command,delete_command
from .views import index_user, get_data_users, delete_user, create_user,edit_user
from .views import get_all_motors, get_all_fuels, get_all_colors, get_all_models, get_all_users
from .views import produit_autocomplete,user_autocomplete,motor_autocomplete

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home, name='home'), 
    path('about/', about, name='about'),
    path('users/', list_users, name='list_users'),
    path('test_html', test_html, name='test_html'),

    path('commands', index_command, name='index_command'),
    path('home', index_command, name='index_command'),
    path('commands/get_data_command', get_data_command, name='get_data_command'),
    path('commands/create_command', create_command, name='create_command'),    
    path('commands/edit', edit_command, name='edit_command'),  
    path('commands/delete', delete_command, name='delete_command'),
    
    path('users', index_user, name='index_user'),
    path('users/get_data_users', get_data_users, name='get_data_users'),
    path('users/create_user', create_user, name='create_user'),
    path('users/delete', delete_user, name='delete_user'),
    path('users/edit', edit_user, name='edit_user'),

    path('motor/auto_complete', motor_autocomplete, name='motor_autocomplete'),
    
    path('get_all_motors', get_all_motors, name='get_all_motors'),
    path('get_all_fuels', get_all_fuels, name='get_all_fuels'),
    path('get_all_colors', get_all_colors, name='get_all_colors'),
    path('get_all_models', get_all_models, name='get_all_models'),
    path('get_all_users', get_all_users, name='get_all_users'),

    path('produit/auto_complete', produit_autocomplete, name='produit_autocomplete'),
    path('users/auto_complete', user_autocomplete, name='user_autocomplete'),


]
