from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from django.db import connection
from django.views.decorators.csrf import csrf_exempt
import json
from neo4j import GraphDatabase
import logging


def home(request):
        return render(request, 'home.html')

def about(request):
    return HttpResponse("This is the about page.")

def list_users(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT idUser, name, surname, mail FROM Users")
        user_data = cursor.fetchall()

    users = [{'id': user[0], 'name': user[1], 'surname': user[2], 'mail': user[3]} for user in user_data]
    return render(request, 'afficher_utilisateurs.html', {'users': users})

def get_mail_by_id(anUserId):
    with connection.cursor() as cursor:
        sql = "SELECT mail FROM Users WHERE idUser = %s"
        cursor.execute(sql, (anUserId,))
        user_email = cursor.fetchone() 

    return user_email[0] if user_email else None

def test_html(request):
    return render(request, 'test.html')

def index_command(request):
    return render(request, 'command/index_command.html')

def get_data_command(request):
    uri = "bolt://neo4j:7687"
    user = "neo4j"
    password = "myneo4jpassword"

    driver = GraphDatabase.driver(uri, auth=(user, password))

    with driver.session() as session:
        query = (
            "MATCH (command:Command)-[:USES_MODEL]->(model)"
            "MATCH (command)-[:USES_MOTOR]->(motor)"
            "MATCH (command)-[:USES_COLOR]->(color)"
            "MATCH (command)-[:USES_FUEL]->(fuel)"
            "RETURN ID(command) AS commandId, "
            "       command.idUser AS userId, "
            "       model.nameModel AS modelName, "
            "       motor.nameMotor AS motorName, "
            "       color.nameColor AS colorName, "
            "       command.IsHyperEspaceProof AS isHyperSpaceProof"
        )
        result = session.run(query)
        
        data = []
        for record in result:
            email = get_mail_by_id(record['userId'])  
            command = {
                'mail': email,
                'modele': record['modelName'],
                'motors': record['motorName'],
                'color': record['colorName'],
                'hyperSpace': 'oui' if record['isHyperSpaceProof'] else 'non',
                'edit': f'<button type="button" class="btn bi bi-pencil-square text-warning" style="font-size: 1.3rem;" onclick="openEditModalCommand({record["commandId"]})"></button>',
                'delete': f'<button type="button" class="btn bi bi-trash3-fill text-danger" style="font-size: 1.3rem;" onclick="deleteCommand({record["commandId"]})"></button>'
            }
            data.append(command)
    
    driver.close() 

    return JsonResponse(data, safe=False)


def create_command(request):
    if request.method == 'POST':
        idUser = request.POST.get('input_user')
        idModel = request.POST.get('input_model')
        idMotor = request.POST.get('input_motor')
        idColor = request.POST.get('input_color')
        idFuel = request.POST.get('input_fuel')
        isHyperEspaceProof = request.POST.get('bool_hyperspace') == 'true'

        uri = "bolt://neo4j:7687"
        user = "neo4j"
        password = "myneo4jpassword"

        with GraphDatabase.driver(uri, auth=(user, password)) as driver:
            with driver.session() as session:
                query = (
                    "CREATE (command:Command {idUser: $idUser, IsHyperEspaceProof: $isHyperEspaceProof})"
                    "WITH command "
                    "MATCH (model:Model {idModel: " + str(idModel) + "})"
                    "MATCH (motor:Motor {idMotor: " + str(idMotor) + "}) "
                    "MATCH (color:Color {idColor: " + str(idColor) + "}) "
                    "MATCH (fuel:Fuel {idFuel: " + str(idFuel) + "}) "
                    "MERGE (command)-[:USES_MODEL]->(model) "
                    "MERGE (command)-[:USES_MOTOR]->(motor) "
                    "MERGE (command)-[:USES_COLOR]->(color) "
                    "MERGE (command)-[:USES_FUEL]->(fuel) "
                    "RETURN command"
                )

                result = session.run(
                    query,
                    idUser=idUser,
                    idModel=idModel,
                    idMotor=idMotor,
                    idColor=idColor,
                    idFuel=idFuel,
                    isHyperEspaceProof=isHyperEspaceProof
                )

        logging.info(f"Query: {query}")
        print(f"Query: {query}")
        logging.debug(f"Received POST data - User: {idUser}, Model: {idModel}, Motor: {idMotor}, Color: {idColor}, Fuel: {idFuel}, HyperSpace: {isHyperEspaceProof}")
        
        response_data = {'message': 'command was successfully created'}
        return JsonResponse(response_data)
    else:
        return JsonResponse({'error': 'Method not allowed'}, status=405)

def edit_color(aCommandId, aColorId):
    uri = "bolt://neo4j:7687"
    user = "neo4j"
    password = "myneo4jpassword"

    with GraphDatabase.driver(uri, auth=(user, password)) as driver:
        with driver.session() as session:
            result = session.run(
                "MATCH (command:Command)-[rel:USES_COLOR]->(:Color) "
                "WHERE id(command) =" + str(aCommandId) + " "
                "DELETE rel "
                "WITH command "
                "MATCH (newColor:Color {idColor:" + str(aColorId) + "}) "
                "MERGE (command)-[:USES_COLOR]->(newColor) "
            )

    logging.debug(f"Edited Command - Command ID: {aCommandId}, Color: {aColorId}")
    resData = {'step_result': 'edit color done'}
    return JsonResponse(resData, safe=False)

def edit_model(aCommandId, aModelId):
    uri = "bolt://neo4j:7687"
    user = "neo4j"
    password = "myneo4jpassword"

    with GraphDatabase.driver(uri, auth=(user, password)) as driver:
        with driver.session() as session:
            result = session.run(
                "MATCH (command:Command)-[rel:USES_MODEL]->(:Model) "
                "WHERE id(command) =" + str(aCommandId) + " "
                "DELETE rel "
                "WITH command "
                "MATCH (newModel:Model {idModel:" + str(aModelId) + "}) "
                "MERGE (command)-[:USES_MODEL]->(newModel) "
            )

    logging.debug(f"Edited Command - Command ID: {aCommandId}, Model: {aModelId}")
    resData = {'step_result': 'edit model done'}
    return JsonResponse(resData, safe=False)

def edit_motor(aCommandId, aMotorId):
    uri = "bolt://neo4j:7687"
    user = "neo4j"
    password = "myneo4jpassword"

    with GraphDatabase.driver(uri, auth=(user, password)) as driver:
        with driver.session() as session:
            result = session.run(
                "MATCH (command:Command)-[rel:USES_MOTOR]->(:Motor) "
                "WHERE id(command) =" + str(aCommandId) + " "
                "DELETE rel "
                "WITH command "
                "MATCH (newMotor:Motor {idMotor:" + str(aMotorId) + "}) "
                "MERGE (command)-[:USES_MOTOR]->(newMotor) "
            )

    logging.debug(f"Edited Command - Command ID: {aCommandId}, Motor: {aMotorId}")
    resData = {'step_result': 'edit motor done'}
    return JsonResponse(resData, safe=False)

def edit_fuel(aCommandId, aFuelId):
    uri = "bolt://neo4j:7687"
    user = "neo4j"
    password = "myneo4jpassword"

    with GraphDatabase.driver(uri, auth=(user, password)) as driver:
        with driver.session() as session:
            result = session.run(
                "MATCH (command:Command)-[rel:USES_FUEL]->(:Fuel) "
                "WHERE id(command) =" + str(aCommandId) + " "
                "DELETE rel "
                "WITH command "
                "MATCH (newFuel:Fuel {idFuel:" + str(aFuelId) + "}) "
                "MERGE (command)-[:USES_FUEL]->(newFuel) "
            )

    logging.debug(f"Edited Command - Command ID: {aCommandId}, Fuel: {aFuelId}")
    resData = {'step_result': 'edit fuel done'}
    return JsonResponse(resData, safe=False)

def edit_User(aCommandId, aUserId):
    uri = "bolt://neo4j:7687"
    user = "neo4j"
    password = "myneo4jpassword"

    with GraphDatabase.driver(uri, auth=(user, password)) as driver:
        with driver.session() as session:
            result = session.run(
                "MATCH (command:Command) "
                "WHERE id(command) = " + str(aCommandId) + " "
                "SET command.idUser = " + str(aUserId) + " "
            )

    logging.debug(f"Edited Command - Command ID: {aCommandId}, User: {aUserId}")
    resData = {'step_result': 'edit user done'}
    return JsonResponse(resData, safe=False)

def edit_ishyperspaceproof(aCommandId, aIsHyperSpaceProof):
    uri = "bolt://neo4j:7687"
    user = "neo4j"
    password = "myneo4jpassword"

    with GraphDatabase.driver(uri, auth=(user, password)) as driver:
        with driver.session() as session:
            result = session.run(
                "MATCH (command:Command) "
                "WHERE id(command) = " + str(aCommandId) + " "
                "SET command.IsHyperEspaceProof = " + str(aIsHyperSpaceProof) + " "
            )

    logging.debug(f"Edited Command - Command ID: {aCommandId}, HyperSpace: {aIsHyperSpaceProof}")
    resData = {'step_result': 'edit hyperSpace done'}
    return JsonResponse(resData, safe=False)


def edit_command(request):
    if request.method == 'POST':
        commandId = request.POST.get('id_edit_command')
        idUser = request.POST.get('input_Edituser')
        idModel = request.POST.get('input_Editmodel')
        idMotor = request.POST.get('input_Editmotor')
        idColor = request.POST.get('input_Editcolor')
        idFuel = request.POST.get('input_Editfuel')
        isHyperEspaceProof = str(request.POST.get('bool_hyperspace')) == 'on'

        edit_color(commandId, idColor)
        edit_model(commandId, idModel)
        edit_motor(commandId, idMotor)
        edit_fuel(commandId, idFuel)
        edit_User(commandId, idUser)
        edit_ishyperspaceproof(commandId, isHyperEspaceProof)

        logging.debug(f"Edited Command - Command ID: {commandId}, Model: {idModel}, User: {idUser}, Motor: {idMotor}, Color: {idColor}, Fuel: {idFuel}, HyperSpace: {isHyperEspaceProof}")
        return JsonResponse({'message': 'command edited successfully'})

    if request.method == 'GET':
        commandId = request.GET.get('id_edit_command')
        data = get_command_by_id(commandId)
        return data
    else:
        return JsonResponse({'error': 'Method not allowed'}, status=405)

def get_command_by_id(aCommandId):
    uri = "bolt://neo4j:7687"
    user = "neo4j"
    password = "myneo4jpassword"

    driver = GraphDatabase.driver(uri, auth=(user, password))

    with driver.session() as session:
        result = session.run(
            "MATCH (command:Command)-[:USES_MODEL]->(model) "
            "MATCH (command)-[:USES_MOTOR]->(motor) "
            "MATCH (command)-[:USES_COLOR]->(color) "
            "MATCH (command)-[:USES_FUEL]->(fuel) "
            "WHERE id(command) =" + str(aCommandId) + " "
            "RETURN command.idUser AS userId, "
            "       model.idModel AS model, "
            "       motor.idMotor AS motor, "
            "       fuel.idFuel AS fuel, "
            "       color.idColor AS color, "
            "       command.IsHyperEspaceProof AS isHyperSpaceProof"
        )  
        
        record = result.single()

        if record:
            data = [{
                'userId': record['userId'],
                'modelId': record['model'],
                'motorId': record['motor'],
                'colorId': record['color'],
                'fuelId': record['fuel'],
                'hyperSpace': record['isHyperSpaceProof']
            }]
        else:
            return JsonResponse({'error': 'No results found'})
    
    driver.close() 

    return JsonResponse(data, safe=False)


def delete_command(request):
    if request.method == 'POST':
        input_id = request.POST.get('input_id')
        
        uri = "bolt://neo4j:7687"
        user = "neo4j"
        password = "myneo4jpassword"

        with GraphDatabase.driver(uri, auth=(user, password)) as driver:
            with driver.session() as session:
                result = session.run(
                    "MATCH (command:Command) "
                    "WHERE id(command) = " + str(input_id) + " "
                    "DETACH DELETE command"
                )

        logging.debug(f"Deleted Command - Command ID: {input_id}")
        response_data = {'message': 'Command has been delete', 'data': input_id}
        return JsonResponse(response_data, safe=False)
    else:
        return JsonResponse({'error': 'Méthode non autorisée'}, status=405)


def index_user(request):
    return render(request, 'user/index_users.html')


def get_data_users(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT idUser, name, surname, mail FROM Users")
        user_data = cursor.fetchall()

    data = [{'id': user[0], 
    'name': user[1], 
    'surname': user[2], 
    'mail': user[3],
    'add': f'<button type="button" class="btn bi bi-cart-plus text-success" style="font-size: 1.3rem;" onclick="openCreateModalCommand({user[0]})"></button>',
    'edit': f'<button type="button" class="btn bi bi-pencil-square text-warning" style="font-size: 1.3rem;" onclick="openEditModalUser({[user[1], user[2], user[3], user[0]]})"></button>',
    'delete': f'<button type="button" class="btn bi bi-trash3-fill text-danger" style="font-size: 1.3rem;" onclick="deleteUser({user[0]})"></button>',
    }
    for user in user_data]
    return JsonResponse(data, safe=False)


def create_user(request):
    if request.method == 'POST':
        input_name = request.POST.get('input_name')
        input_surname = request.POST.get('input_surname')
        input_mail = request.POST.get('input_mail')

        try:
            with connection.cursor() as cursor:
                cursor.execute("INSERT INTO Users (name, surname, mail) VALUES ('" + str(input_name) + "','" + str(input_surname) + "','"+ str(input_mail) + "')")
                
                return JsonResponse({'message': 'User created successfully'})
        
        except Exception as e:
            return JsonResponse({'error': str(e)}, status=500)
    else:
        return JsonResponse({'error': 'Method not allowed'}, status=405)


def delete_user(request):
    if request.method == 'POST':
        input_id = request.POST.get('input_id')

        try:
            with connection.cursor() as cursor:
                cursor.execute("DELETE FROM Users WHERE idUser = %s", [input_id])
                
                return JsonResponse({'message': 'User deleted successfully', 'user_id': input_id})
        
        except Exception as e:
            return JsonResponse({'error': str(e)}, status=500)
    else:
        return JsonResponse({'error': 'Method not allowed'}, status=405)
        
        
from django.http import JsonResponse
from django.db import connection

def edit_user(request):
    if request.method == 'POST':
        input_name = request.POST.get('inputEdit_name')
        input_surname = request.POST.get('inputEdit_surname')
        input_mail = request.POST.get('inputEdit_mail')
        input_id = request.POST.get('id_edit_user')

        try:
            with connection.cursor() as cursor:
                cursor.execute("UPDATE Users SET name ='" +  input_name + "', surname ='" +  input_surname + "', mail ='" +  input_mail + "' WHERE iduser = " +  input_id)
                
                return JsonResponse({'message': 'User edited successfully', 'user_id': input_id})
        
        except Exception as e:
            return JsonResponse({'error': str(e)}, status=500)
    else:
        return JsonResponse({'error': 'Method not allowed'}, status=405)


def motor_autocomplete(request):
        data = [
        {'value': 1, 'label': 'GAZ'},
        {'value': 2, 'label': 'NUCLEAR'},
        ]
        return JsonResponse(data, safe=False)

def user_autocomplete(request):
    if request.method == 'GET':
        term = request.GET.get('term')
        # return JsonResponse({'data' : term}, safe=False)
        with connection.cursor() as cursor:
            cursor.execute("SELECT idUser, CONCAT_WS('-', name, surname, mail) AS concatenated_info FROM Users WHERE CONCAT_WS('-', name, surname, mail) LIKE %s", ['%' + term + '%'])
            user_data = cursor.fetchall()
        data = [{'value': user[0], 'label': user[1]} for user in user_data]
        return JsonResponse(data, safe=False)

    
        
def produit_autocomplete(request):
        data = [
        {'value': 1, 'label': 'GAZ'},
        {'value': 2, 'label': 'NUCLEAR'},
        ]
        return JsonResponse(data, safe=False)

def get_all_motors(request):
    uri = "bolt://neo4j:7687"
    user = "neo4j"
    password = "myneo4jpassword"

    with GraphDatabase.driver(uri, auth=(user, password)) as driver:
        with driver.session() as session:
            result = session.run("MATCH (m:Motor) RETURN m.idMotor as idMotor, m.nameMotor AS name, m.power AS power, m.priceMotor AS price")
            motors = [dict(record) for record in result]
            
    return JsonResponse({'motors': motors})

def get_all_fuels(request):
    uri = "bolt://neo4j:7687"
    user = "neo4j"
    password = "myneo4jpassword"

    with GraphDatabase.driver(uri, auth=(user, password)) as driver:
        with driver.session() as session:
            result = session.run("MATCH (f:Fuel) RETURN f.idFuel as idFuel, f.nameFuel AS name, f.isGreen AS isGreen")
            fuels = [dict(record) for record in result]
            
    return JsonResponse({'fuels': fuels})

def get_all_colors(request):
    uri = "bolt://neo4j:7687"
    user = "neo4j"
    password = "myneo4jpassword"

    with GraphDatabase.driver(uri, auth=(user, password)) as driver:
        with driver.session() as session:
            result = session.run("MATCH (c:Color) RETURN c.idColor as idColor, c.nameColor AS name")
            colors = [dict(record) for record in result]

    return JsonResponse({'colors': colors})

def get_all_models(request):
    uri = "bolt://neo4j:7687"
    user = "neo4j"
    password = "myneo4jpassword"

    with GraphDatabase.driver(uri, auth=(user, password)) as driver:
        with driver.session() as session:
            result = session.run("MATCH (m:Model) RETURN m.idModel as idModel, m.nameModel AS name, m.priceModel AS price")
            models = [dict(record) for record in result]
            
    return JsonResponse({'models': models})

def get_all_users(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT idUser, name, surname FROM Users")
        users = cursor.fetchall()
        user_data = [{'idUser': user[0], 'name': user[1], 'surname': user[2]} for user in users]

    return JsonResponse({'users': user_data})

