# Utilisez une image Python officielle en tant que base
FROM python:3.8

# Créez le répertoire de travail et copiez les fichiers requis
WORKDIR /app
COPY requirements.txt /app/
COPY . /app/

# Installez les dépendances
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

# Exposez le port nécessaire pour Django
EXPOSE 8000

# Lancez le serveur Django
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]